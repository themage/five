<?php

class Automacao {

    public function AutoLoad($Arquivo, $dados = null) {

        $arquivo = 'views/' . $Arquivo . '.php';
        if (file_exists($arquivo)) {
            ///Verifica se é modal
            $parametros = explode('_', $Arquivo);
            $p1 = isset($parametros[1]) ? $parametros[1] : null;
            $p2 = isset($parametros[2]) ? $parametros[2] : null;
            include_once $arquivo;

            if ($p1 == "acao" || $p2 == "acao" || $p1 == 'config' || $p2 == 'config') {
                // echo "modal";
                echo "<script>"
                . "$(document).ready(function () {";
                echo "ativaBotaoSalvar();";
                echo " });</script>";
            }
        } else {
            include_once "pages/erro_404.php";
        }
    }

}
