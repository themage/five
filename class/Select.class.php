<?php

function trim_value(&$value) {
    $value = trim($value);
}

class Select {

    public function geraQuery(array $colunas, $tabela, array $condicao = null, $ordenacao = null, $outros = null, $idCasa = null) {
        $cone = new Database();
        $conexao = $cone->Begin();
        // $conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //Verifica se a tabela são universais
        /*$existe = array_search($tabela, TBL_S_USER, FALSE);
        //var_dump($existe);
        if ($existe === FALSE) {
            ///Verifica se a ID foi passada
            if ($idUser == null) {
                $idUser = $_SESSION['frlIdUser'];
            }
        }*/
        $painel = ''; 
        
        //Remove os espaços
        array_walk($colunas, 'trim_value');


        ///// Novo Jeito
        //Gera as colunas de retorno e já as apelidas
        $nm_col = '';
        foreach ($colunas as $key) {
            //Concatena o nome da tabela com o nome do campo e já da o apelido
            $nm_col = $nm_col . " " . $key;
            if ($colunas[count($colunas) - 1] <> $key) {
                $nm_col = $nm_col . ", ";
            }
        }

        //// fim 
        ///Cria a condição
        $cond = "";
        if ($condicao <> null) {
            $i2 = 0;
            for ($i = 0; $i < count($condicao);) {

                $cond = $cond . $condicao[$i++] . " " . $condicao[$i++] . " ? ";

                $valores_cond[$i2] = $condicao[$i++];
                if (count($condicao) != $i) {
                    if (isset($cond[$i])) {
                        $cond = $cond . $condicao[$i++] . " ";
                    }
                    $i2++;
                }
            }
        }



        if ($outros <> null) {
            $outros = $outros . " , ";
        }
        $where = $cond == null ? " " : " WHERE ";
        $query = "SELECT " . $outros . $nm_col . " FROM " . $painel . $tabela . $where . $cond . " " . $ordenacao;
        //echo $query;
        //return;
        $stmt = $conexao->prepare($query);
        if ($condicao != null) {
            $i2 = 1;
            for ($i = 0; $i < count($valores_cond);) {
                $stmt->bindValue($i2, $valores_cond[$i]);
                $i2++;
                $i++;
            }
        }
        // $cone->Commit();
        //echo $query;
        return $stmt;
    }

    public function SelectP(array $colunas, $tabela, array $condicao = null, $ordenacao = null, $outros = null, $idCasa = null) {

        $smtp = $this->geraQuery($colunas, $tabela, $condicao, $ordenacao, $outros, $idCasa);
        try {
            //var_dump($smtp);
            if ($smtp->execute()) {
                $results = $smtp->fetchAll(PDO::FETCH_ASSOC);
                return $results;
            }
        } catch (Exception $exc) {
            //var_dump($exc);
            $log = new Log_mo();
            $log->Inserir("erro_select", "pdo_02", $exc, $exc->getMessage());
            return false;
        } catch (PDOException $exc) {
            //var_dump($exc);
            $log = new Log_mo();
            $log->Inserir("erro_select", "pdo_03", $exc, $exc->getMessage());
            return false;
        }
    }

    public function SelectQuery($query) {
        $conexao = new Database();
        $conexao->Begin();
        $stmt = $conexao->conexao()->prepare($query);
        if ($stmt->execute()) {
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $results;
        } else {
            return false;
        }
    }

    public function Count($tabela, array $condicao = null, $ordenacao = null, $outros = null, $idCasa = null, array $colunas = ['id']) {
        $smtp = $this->geraQuery($colunas, $tabela, $condicao, $ordenacao, $outros, $idCasa);

        if ($smtp->execute()) {
            $results = $smtp->rowCount();
            return $results;
        } else {
            return "E | Ocorreu um erro na execução da consulta";
        }
    }

    public function CountQury(string $Query) {
        $conexao = Database::conexao();
        $smtp = $conexao->prepare($Query);
        if ($smtp->execute()) {
            $results = $smtp->rowCount();
            return $results;
        } else {
            return "E | Ocorreu um erro na execução da consulta";
        }
    }

    public function Ultima($tabela) {

        $colunas = array("id");
        $smtp = $this->geraQuery($colunas, $tabela, null, " ORDER BY " . $tabela . "_id DESC LIMIT 1");
        if ($smtp->execute()) {
            foreach ($smtp as $row) {
                return $row['id'];
            }
        } else {
            return "E | Ocorreu um erro na execução da consulta";
        }
    }

}

/*
  //print_r(seleciona::select_s("nm, desc", "ct_custo", "nm , = , MENSAL   , AND , ativo , =,  1 , AND , ativo , =,  1"));
  //print_r(seleciona::select_s("nm, desc", "ct_custo"));


  include './Database.class.php';
  include '../config/Config2.php';
  $sele = new Select();

  var_dump($sele->SelectP(['id'], 'usuario', ['id', '=', '6']));
 */
?>
