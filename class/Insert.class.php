<?php

function trim_value2(&$value) {
    $value = trim($value);
}

class Insert {

    function Insere($conexao, array $colunas, array $valores, $tabela, $retId = false) {

        $nm_col = "";
        $val_stm = "";
        //Remove os espaços
        array_walk($valores, 'trim_value2');
        array_walk($colunas, 'trim_value2');

            
        $painel = "";
       
        $nm_col = implode(",", $colunas);
        $val_stm = ":" . implode(",:", $colunas);

        $query = "INSERT INTO " . $painel . $tabela . " (" . $nm_col . " ) VALUES (" . $val_stm . " ) ;";
        // echo $query;

        try {

            ///Faz a preparação do insert
            if (!$stmt = $conexao->prepare($query)) {
                throw new Exception(false);
            }

            for ($i = 0; $i < count($colunas);) {
                $stmt->bindParam(':' . $colunas[$i], $valores[$i]);
                $i++;
            }

            if ($stmt->execute()) {
                $lastId = $conexao->lastInsertId();
                if ($retId) {
                    return $lastId;
                }
                return True;
            } else {
                throw new PDOException(false);
                //return;
            }
        } catch (PDOExecption $pdo) {
            //var_dump($pdo);
            $log = new Log_mo();
            $log->Inserir("erro_insert", "pdo_01", $e, $query);
            //$conexao::Rollback();
            return False;
        } catch (Exception $e) {
            //var_dump($e);
            $log = new Log_mo();
            $log->Inserir("erro_insert", "exec_01", $e, $query);
            //$conexao::Rollback();
            return False;
        }
    }

}

/*
  include_once './Database.class.php';
  include_once '../config/Config2.php';

  $inserir = new Insert();
  $col = array("nome", "nomearquivo");
  $val = array("Alimentaçãos", "toda alimentação");

  $db = new Database();
  $trans = $db->Beguin();
  //Insert Certo
  $errp = 0;
  if (!$inserir->Insere($trans, ["nome", "nomearquivo"], ["Certo", "Certo"], "acesso")) {
  $errp = 1;
  }
  //Insert errado
  if (!$inserir->Insere($trans, $col, $val, "acesso")) {
  $errp = 1;
  }

  if ($errp == 1) {
  $db->Rollback();
  } else {
  $db->Commit();
  }

  //$log = new Log_mo();
  //$log->Inserir("erro_insert", "exec_01", '$e', '$query');
 * 
 */
?>
