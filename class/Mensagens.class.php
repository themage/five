<?php

class Mensagens {

    private $Mensagens;

    public function __construct() {

        $this->Mensagens['pt_BR'] = [
            'nmVazio' => ['TP' => 'A', 'MSG' => 'O Nome não pode ser vazio.'],
            'mnPai' => ['TP' => 'A', 'MSG' => 'O Menu pai nao pode ser o proprio menu.'],
            'nmArquivoVazio' => ['TP' => 'A', 'MSG' => 'O Nome do arquivo não pode ser vazio.'],
            'nmExiste' => ['TP' => 'A', 'MSG' => 'O Nome já exíste.'],
            'nmClasseVazio' => ['TP' => 'A', 'MSG' => 'A classe não pode ser vazia.'],
            'nmClasseExiste' => ['TP' => 'A', 'MSG' => 'A classe já existe.'],
            'erroUpload' => ['TP' => 'A', 'MSG' => 'Ocorreu um erro no upload do arquivo.'],
            'nmArquivoExiste' => ['TP' => 'A', 'MSG' => 'O acesso já exíste.'],
            'arquivoInvalido' => ['TP' => 'A', 'MSG' => 'Arquivo inválido.'],
            'inSucesso' => ['TP' => 'S', 'MSG' => 'Inserido com sucesso.', 'ID' => '', 'ATU' => "TRUE"],
            'erroAtualizar' => ['TP' => 'E', 'MSG' => 'Ocorreu um erro ao atualizar o registro.'],
            'atSucesso' => ['TP' => 'S', 'MSG' => 'Registro atualizado com sucesso.', 'ATU' => "TRUE"],
            'nhSucesso' => ['TP' => 'A', 'MSG' => 'Nenhuma alteração foi realizada.'],
            'erroConsulta' => ['TP' => 'E', 'MSG' => 'Ocorreu um erro ao efetuar a consulta.'],
            'inErro' => ['TP' => 'E', 'MSG' => 'Estamos ausente no momento.'],
            'Redirecinamento' => ['TP' => 'S', 'MSG' => 'Você será redirecionado.', 'ATU' => "TRUE"],
            'erroPermissao' => ['TP' => 'E', 'MSG' => 'Você não tem permissão para acessar esta página!'],
            'logado' => ['TP' => 'S', 'MSG' => 'Você será redirecionado!', 'ATU' => "TRUE"],
            'erroLoginSenha' => ['TP' => 'A', 'MSG' => 'Login ou Senha incorreto!'],
            'erroSiglaVazia' => ['TP' => 'A', 'MSG' => 'A sigla não pode ser vazia!!'],
            'erroVoltagemVazia' => ['TP' => 'A', 'MSG' => 'A voltagem não pode ser vazia!!'],
            'erroAmperagemVazia' => ['TP' => 'A', 'MSG' => 'A amperagem não pode ser vazia!!'],
            'exSucesso' => ['TP' => 'S', 'MSG' => 'Item excluido com sucesso!!', 'ATU' => "TRUE"],
            'exNenhum' => ['TP' => 'A', 'MSG' => 'Nenhum item excluido!!'],
            'nmDaCAexiste' => ['TP' => 'A', 'MSG' => 'Nenhum item excluido!!'],
            'regNPSE' => ['TP' => 'A', 'MSG' => 'O registro não pode ser excluido, pois está em uso!!'],
            'userInativo' => ['TP' => 'A', 'MSG' => 'Usuário inativo!!'],
            'emailVazio' => ['TP' => 'A', 'MSG' => 'Campo email não pode ser vazio!!'],
            'SelclasseDeacesso' => ['TP' => 'A', 'MSG' => 'Selecione uma classe de acesso!!'],
            'loginVazio' => ['TP' => 'A', 'MSG' => 'Campo login não pode ser vazio!!'],
            'emailExiste' => ['TP' => 'A', 'MSG' => 'Email já cadastrado!!'],
            'senhaVazia' => ['TP' => 'A', 'MSG' => 'Campo senha não pode ser vazio!!'] ,
            'senhaNaoConferem' => ['TP' => 'A', 'MSG' => 'O campo de senha não confere com o campo de confirmação!!'] ,
            'identVazia' => ['TP' => 'A', 'MSG' => 'O campo de identificação não pode ser vazio!!'] ,
            'selTpArduino' => ['TP' => 'A', 'MSG' => 'Selecione o tipo de arduino!!'] ,
            'selIcon' => ['TP' => 'A', 'MSG' => 'Selecione algum icone!!'] ,
            'selTpSensor' => ['TP' => 'A', 'MSG' => 'Selecione um tipo de sensor!!'] ,
            'selTpMed' => ['TP' => 'A', 'MSG' => 'Selecione um tipo de medição!!'] ,
            'inQtdPD' => ['TP' => 'A', 'MSG' => 'Informe a quantidade de portas digitais!!'] ,
            'inQtdPA' => ['TP' => 'A', 'MSG' => 'Informe a quantidade de analógicas !!'] ,
            'delMenu' => ['TP' => 'A', 'MSG' => 'Selecione um menu!!'] ,
            'selArduino' => ['TP' => 'A', 'MSG' => 'Selecione um arduíno!!'] ,
            'selTipoItem' => ['TP' => 'A', 'MSG' => 'Selecione o tipo do item!!'] ,
            'selPortaLiga' => ['TP' => 'A', 'MSG' => 'Selecione uma porta de ligação!!'] ,
            'P1EmUso' => ['TP' => 'A', 'MSG' => 'Porta principal já está em uso!!'] ,
            'inCond' => ['TP' => 'A', 'MSG' => 'Informe alguma condição!!'] ,
            'inNextExec' => ['TP' => 'A', 'MSG' => 'Informe a data de execução!!'] ,
            'inItem' => ['TP' => 'A', 'MSG' => 'Selecione pelo menos um item!!'] ,
            'tpSensorJaExiste' => ['TP' => 'A', 'MSG' => 'Tipo de sensor já existe!!'],
            'tpCursoVazio' => ['TP' => 'A', 'MSG' => 'Selecione o tipo do curso.'],
            'tgVazio' => ['TP' => 'A', 'MSG' => 'Campo "Tag" está vazio.'],
            'tgExiste' => ['TP' => 'A', 'MSG' => 'Campo "Tag" já existe.'],
            'mcVazio' => ['TP' => 'A', 'MSG' => 'Selecione a forma de contato.'],
            'ncVazio' => ['TP' => 'A', 'MSG' => 'Selecione o cliente.']
        ];
    }

    public function Msg($msg) {
        return $this->Mensagens[LINGUAGEM][$msg];
    }

}

/*
  ----------------------   Instruções ----------------------------------
 * Como criar a função: $m = new Mensagens();
 * Passe via parâmetro  o indíce da msg que deseja; ex: $m->Msg('inSucesso');
 * 
 */
?>
