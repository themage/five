<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">


    <!-- Animate.css -->
    <link rel="stylesheet" href="<?= HOST ?>/assets/soon/css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="<?= HOST ?>/assets/soon/css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="<?= HOST ?>/assets/css/argon.css?v=1.2.0" type="text/css">
    <!-- Theme style  -->
    <link rel="stylesheet" href="<?= HOST ?>/assets/soon/css/style.css">



</head>
<body>
    <div class="header bg-gradient-info pb-6">
        <div class="container-fluid">
            <div class="header-body">
                
                <div class="row align-items-center py-4">
                    <h6 class="h2 text-white text-uppercase d-inline-block mb-0">EM BREVE</h6>
                </div>
                
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="display-t js-fullheight">
                            <div class="simply-countdown simply-countdown-one"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="<?= HOST ?>/assets/soon/js/jquery.min.js"></script>

    <!-- jQuery Easing -->
    <script src="<?= HOST ?>/assets/soon/js/jquery.easing.1.3.js"></script>
    <!-- Bootstrap -->
    <script src="<?= HOST ?>/assets/soon/js/bootstrap.min.js"></script>
    <!-- Waypoints -->
    <script src="<?= HOST ?>/assets/soon/js/jquery.waypoints.min.js"></script>

    <!-- Count Down -->
    <script src="<?= HOST ?>/assets/soon/js/simplyCountdown.js"></script>
    <!-- Main -->



    <script>
        var d = new Date(new Date().getTime() + 1000 * 120 * 120 * 2000);

        // default example
        simplyCountdown('.simply-countdown-one', {
            year: 2020,
            month: 12,
            day: 1,
            hours: 20,
            minutes: 0,
            seconds: 0
        });

        //jQuery example
        $('#simply-countdown-losange').simplyCountdown({
            year: d.getFullYear(),
            month: d.getMonth() + 1,
            day: d.getDate(),
            enableUtc: true
        });
    </script>

</body>
</html>

