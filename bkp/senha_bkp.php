<div class="header bg-gradient-info pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white text-uppercase d-inline-block mb-0">painel de senhas</h6>
                </div>                
            </div>


            <div class="row">
                <!-- Informativos -->
                <div class="col-xl-6 col-md-8">
                    <div class="card card-stats">
                        <!-- Card body -->
                        <div class="card-body">
                            <div class="row">
                               
                                <div class="col">
                                    <span class="h2 font-weight-bold mb-0">Senha Normal</span>
                                </div>                                
                            </div>
                            <div class="row">
                                 <div class="col-lg-6 col-5 text-right">
                                    <a href="#"><i class="btn  btn btn-info btn-lg btn-block abrirModal " data-toggle="modal" data-target=".modal-cadastros"  data-title="Nova senha "  data-type="senha/Gerar">Novo</i></a>             
                                </div>
                                <!--                                
                                  <div class="col">
                                  <button type="button" class="btn btn-info btn-lg btn-block">Gerar Senha</button>
                                  </div>-->
                            </div>
                        </div>
                    </div>
                </div>



                <div class="col-xl-6 col-md-8">
                    <div class="card card-stats">
                        <!-- Card body -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <span class="h2 font-weight-bold mb-0">Senha Preferencial</span>
                                </div>                                
                            </div>
                            <div class="row">
                                <div class="col">
                                    <button type="button" class="btn btn-warning btn-lg btn-block">Gerar Senha</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

