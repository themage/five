<?php

//echo var_dump($_SERVER);
//Define Link
define("HOST", "http://" . $_SERVER['HTTP_HOST'] . "/five");

//Define a variavel global LINGUAGEM e atribui a LINGUAGEM  do site;
define("LINGUAGEM", "pt_BR");
//Define a linguagem do site
setlocale(LC_ALL, LINGUAGEM);

///encoding
define("ENCODING", "UTF-8");

//Define o Time Zone
date_default_timezone_set('America/Manaus');







/////Define as colunas das tabelas
$NM_COL = array(
    "usuario" => ["id", "nome","email","imagem","ativo"],
    "departamento" => ['id','nome','sigla','descricao','ativo'],
    "painel" => ['id','tempo'],
    "configuracao" => ['id','tempo'],
    "senha" => ['id','departamentoid','numero'],
    "atendimento" => ['id','clienteid','solicitante','motivo','meio_contato','contato','situacao','userid','data', 'ativo'],
    "cliente" => ['id','nome','apelido','cidade','telefone1','telefone2','celular','observacao','ativo']
    
    
    
);
define("NM_COL", $NM_COL);

//Inicializa a session
ini_set('session.cookie_lifetime', 60 * 60 * 24 * 7);
session_start();
/*
////Tabelas sem usuario e sem id_home
define("TBL_S_USER", ["-1", "usuario", "acesso", "log", "classe_acesso", "no_access", 'icone', 'menu', 'menu_admin', 'curso', 'alunos', 'eventos', 'tipo_curso', 'tag', 'post', 'post_tag', 'bolsa', 'depoimento', 'editais']);

//Tabelas que são da schemas admin
define("TBL_S_ADMIN", ["-1", "usuario", "acesso", "log", "classe_acesso", "no_access", 'menu_admin', 'alunos', 'eventos', 'tag']);

//Tabelas que possuem a id do usuario e data que cadastrou e editou
define("TBL_CAD_EDITOU", ["-1", "usuario", "acesso",  "classe_acesso", 'menu_admin', 'alunos', 'eventos', 'icone', 'menu', 'curso', 'tipo_curso', 'layout', 'tag', 'post', 'bolsa', 'depoimento', 'editais']);
*/

///Função auto load
spl_autoload_register(
        function ($arquivo) {

    $caminho = [
        "controllers/" . $arquivo . ".php",
        "models/" . $arquivo . ".php",
        "class/" . $arquivo . ".class.php",
        "controllers/" . $arquivo . ".php",
        "models/" . $arquivo . ".php",
        "class/" . $arquivo . ".class.php",
        "../controllers/" . $arquivo . ".php",
        "../models/" . $arquivo . ".php",
        "../class/" . $arquivo . ".class.php",
        "../../controllers/" . $arquivo . ".php",
        "../../models/" . $arquivo . ".php",
        "../../class/" . $arquivo . ".class.php"
    ];

    foreach ($caminho as $key => $value) {
        if (file_exists($value)) {
            include_once $value;
            break;
        }
    }
});


/// Para abertura dos modals
$url = str_replace("Novo/", "", $_SERVER["REQUEST_URI"]);
//var_dump($url) ;
//echo $url;
$url = explode("/", $url);



///------------------------Carrega os niveis de perimição ------------
$user = new Usuario_mo();
//var_dump($user->getLogado());
/*if (isset($_SESSION['frlIdCa'])) {
    $pemi = array(-1);
    foreach ($user->getNoAccess($_SESSION['frlIdCa']) as $value) {
        $pemi[$value['nomearquivo']] = TRUE;
    };
    define("NO_ACCESS", $pemi);
}*/
?>