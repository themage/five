
<!-- Sidenav -->
<nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header d-flex align-items-center">
            <a class="navbar-brand" href="javascript:void(0)">Five Software
                <img src="<?= HOST ?>/assets/img/brand/icon.png" class="navbar-brand-img" alt="...">
            </a>
            <div class="ml-auto">
                <!-- Sidenav toggler -->
                <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
                    <div class="sidenav-toggler-inner">
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar-inner">
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <!-- Nav items -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="<?= HOST ?>/home/index.html">
                            <i class="ni ni-tv-2 text-primary"></i>
                            <span class="nav-link-text">Home</span>
                        </a>
                    </li>                     
                    <li class="nav-item">
                        <a class="nav-link" href="<?= HOST ?>/usuario/index.html">
                            <i class="ni ni-circle-08 text-pink"></i>
                            <span class="nav-link-text ">Usuario</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= HOST ?>/cliente/index.html">
                            <i class="ni ni-circle-08 text-success"></i>
                            <span class="nav-link-text ">Clientes</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= HOST ?>/atendimento/index.html">
                            <i class="ni ni-archive-2 text-warning"></i>
                            <span class="nav-link-text">Atendimentos</span>
                        </a>
                    </li>                    
                    <li class="nav-item">
                        <a class="nav-link" href="<?= HOST ?>/registro/index.html">
                            <i class="ni ni-archive-2 text-warning"></i>
                            <span class="nav-link-text">Registros</span>
                        </a>
                    </li>
                    

                </ul>

            </div>
        </div>
    </div>
</nav>
