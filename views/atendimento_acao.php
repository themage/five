<?php
//var_dump($dados);
if (isset($dados['acao']) && $dados['acao'] == 'editar' || $dados['acao'] == 'excluir') {
    $registros = $dados['dados'];
    foreach ($registros as $value) {
        ?>
        <script>
            $(document).ready(function () {
                //auto preenchimento
//                $("input[name='apelido']").blur(function () {
//                    var $nome = $("input[name='nome']");                    
//                    $.getJSON('function.php', {
//                        apelido: $(this).val()
//                    }, function (json) {
//                        $nome.val(json.nome);                        
//                    });
//                });


                $("#Acao").val("<?= $dados['acao']; ?>");
                $("#idRegistro").val("<?= $value['id']; ?>");
                if ("<?= $dados['acao']; ?>" == "editar") {
        <?php
        foreach ($value as $key => $valor) {
            if ($key == "ativo" && $valor == 0) {
                echo "$('#$key').attr('checked', false);";
            } else {
                echo "$('#$key').val('$valor');";
            }
        }
        ?>

                } else {
                    $(".formulario").html("Confirma a exclusão do registro?");
                }
            });
        </script>
        <?php
    }
}
?>



<form id="formulario"  enctype="multipart/form-data">
    <input value="<?= HOST ?>/post/<?= $dados['nm_class_mani']; ?>/gravar.html" id="url" type="hidden">
    <input value="novo" id="Acao" name="Acao" type="hidden">
    <input value="0" id="idRegistro" name="idRegistro" type="hidden">

    <span class="formulario">
        <div class="form-row">

            <div class="form-group col-md-8">
                <label for="clienteid">Cliente</label>
                <select name="clienteid" class="form-control" id="clienteid">
                    <option value='0'>Selecione o Cliente</option>
                    <?php
                    foreach ($dados['cliente'] as $value) {
                        echo " <option value='{$value['id']}'>{$value['apelido']}</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-md-4">
                <label for="solicitante">Nome do solicitante</label>
                <input type="text" class="form-control text-uppercase" maxlength="20"  id="solicitante" name="solicitante" placeholder="Nome do solicitante">
            </div>
        </div>
        
        <div class="form-group">
            <label for="motivo">Motivo da solicitação</label>
            <textarea class="form-control text-uppercase" id="motivo" name="motivo" rows="3"></textarea>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="meio_contato">Meio de contato</label>
                <select class="form-control text-uppercase" id="meio_contato" name="meio_contato">
                    <option value='0'>Seleciona a Opção</option>
                    <option>Celular</option>
                    <option>Fixo</option>
                    <option>Visita</option>
                    <option>Whatsapp</option>
                    <option>Outros</option>
                </select>
            </div>

            <div class="form-group col-md-6">
                <label for="contato">Contato</label>
                <input type="text" class="form-control upper-info" maxlength="15" id="contato" name="contato" placeholder="contato">
            </div>              
        </div>
        <div class="form-group">
            <label for="situacao">Situação</label>
            <select class="form-control" id="situacao" name="situacao">
                <option>Finalizado</option>
                <option>Aguardo</option>
                <option>Remarcado</option>                
            </select>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="ativo" name="ativo" checked="true">
            <label class="form-check-label" for="ativo" value="true">
                Ativo
            </label>
        </div>
    </span>
</form>