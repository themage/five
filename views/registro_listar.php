<div class="header ">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-black d-inline-block mb-0"> <?= $dados['titulo']; ?></h6>
                </div>
            </div>
            <div class="table-responsive ">

                <div>
                    <p class="text-muted font-13 m-b-30">
                            Use os botões para exportar os dados. 
                        </p>
                    <table class="table table-flush " id="datatable-buttons"  style="width:100%" >
                        
                        <thead class="thead-light">
                            <tr>
                                <th scope="col" class="sort"  data-sort="id">ID</th>
                                <th scope="col" class="sort" data-sort="apelido">Cliente</th>                                
                                <th scope="col" class="sort" data-sort="motivo">Realizado no Atendimento</th>
                                <th scope="col" class="sort" data-sort="meio_contato">Meio de Contato</th>
                                <th scope="col" class="sort" data-sort="nomeUser">ID do usurio</th>                                
                                <th scope="col" class="sort" data-sort="data">Data</th>                               
                                <th>Status</th>
                                <th>Ações</th>

                            </tr>
                        </thead>
                        <tbody class="list">
                            <?php
                            foreach ($dados['listagem'] as $u) {
                                ?>
                                <tr>
                                    <td><?= strtoupper($u['id']) ?></td>
                                    <td><?= mb_strimwidth(strtoupper($u['apelido']), 0, 22, "...") ?></td>                                    
                                    <td><?= mb_strimwidth(strtoupper($u['motivo']), 0, 30, "...")  ?></td>
                                    <td><?= ($u['meio_contato']) ?></td>
                                    <td><?= ($u['userid']) ?></td>                                    
                                    <td><?= mb_strimwidth ($u['data'], 0, 16) ?></td>
                                    
                                    <td>
                                        <?php
                                        if ($u['situacao'] == 'Aguardo') {
                                            ?>
                                        <i class="fas fa-exclamation-circle fa-2x text-warning"></i>                                            
                                            <?php
                                        } elseif ($u['situacao'] == 'Remarcado') {
                                            ?>
                                            <i class="fas fa-history fa-2x text-yellow"></i>
                                            <?php
                                        } else {
                                            ?>
                                            <i class="far fa-check-circle fa-2x text-success"></i>
                                            <?php
                                        }
                                        ?>
                                    </td>

                                    <td class="text-right">
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                <a href="#" title="Visualizar" class="dropdown-item abrirModalNormal" data-toggle="modal" data-target=".modal-normal" data-title="Visualizar registro."  data-type="<?= $dados['nm_class_mani']; ?>/acao/editar/<?= $u['id'] ?>.html">Visualizar</a> 
                                            </div>
                                        </div>
                                    </td>

                                </tr> 
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
