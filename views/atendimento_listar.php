<div class="header ">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-black d-inline-block mb-0"> <?= $dados['titulo']; ?></h6>
                </div>
                <div class="col-lg-6 col-5 text-right">
                    <a href="#"><i class="btn btn-outline-success  abrirModal " data-toggle="modal" data-target=".modal-cadastros"  data-title="Novo <?= $dados['titulo']; ?>"  data-type="<?= $dados['nm_class_mani']; ?>/acao/novo.html">Novo</i></a>             
                </div>
            </div>
            <div class="table-responsive ">

                <div>
                    <p class="text-muted font-13 m-b-30">
                            Use os botões para exportar os dados. 
                        </p>
                    <table class="table table-flush " id="datatable-buttons"  style="width:100%" >
                        
                        <thead class="thead-light">
                            <tr>
                                <th scope="col" class="sort"  data-sort="id">ID</th>
                                <th scope="col" class="sort" data-sort="apelido">Cliente</th>
                                <th scope="col" class="sort" data-sort="solicitante">Solicitante</th>
                                <th scope="col" class="sort" data-sort="motivo">Realizado no Atendimento</th>
                                <th scope="col" class="sort" data-sort="meio_contato">Meio de Contato</th>
                                <th scope="col" class="sort" data-sort="contato">Contato</th>                                
                                <th scope="col" class="sort" data-sort="data">Data</th>                               
                                <th>Status</th>
                                <th>Ações</th>

                            </tr>
                        </thead>
                        <tbody class="list">
                            <?php
                            foreach ($dados['listagem'] as $u) {
                                ?>
                                <tr>
                                    <td><?= strtoupper($u['id']) ?></td>
                                    <td><?= mb_strimwidth(strtoupper($u['apelido']), 0, 22, "...") ?></td>
                                    <td><?= strtoupper($u['solicitante']) ?></td>
                                    <td><?= mb_strimwidth(strtoupper($u['motivo']), 0, 30, "...")  ?></td>
                                    <td><?= ($u['meio_contato']) ?></td>
                                    <td><?= ($u['contato']) ?></td>                                    
                                    <td><?= mb_strimwidth ($u['data'], 0, 16) ?></td>
                                    
                                    <td>
                                        <?php
                                        if ($u['situacao'] == 'Aguardo') {
                                            ?>
                                        <i class="fas fa-exclamation-circle fa-2x text-warning"></i>                                            
                                            <?php
                                        } elseif ($u['situacao'] == 'Remarcado') {
                                            ?>
                                            <i class="fas fa-history fa-2x text-yellow"></i>
                                            <?php
                                        } else {
                                            ?>
                                            <i class="far fa-check-circle fa-2x text-success"></i>
                                            <?php
                                        }
                                        ?>
                                    </td>

                                    <td class="text-right">
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                <a href="#" title="Editar" class="dropdown-item abrirModal" data-toggle="modal" data-target=".modal-cadastros" data-title="Editar registro."  data-type="<?= $dados['nm_class_mani']; ?>/acao/editar/<?= $u['id'] ?>.html">Editar</a>                                                
                                                <a href="#" title="Excluir" class="dropdown-item abrirModalConfirmacao" data-toggle="modal" data-target=".modal-confirmacao" data-title="Exclusao de Registro" data-type="<?= $dados['nm_class_mani']; ?>/acao/excluir/<?= $u['id'] ?>/.html">Excluir</a>

                                            </div>
                                        </div>
                                    </td>

                                </tr> 
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
