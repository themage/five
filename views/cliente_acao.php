<?php
//var_dump($dados);
if (isset($dados['acao']) && $dados['acao'] == 'editar' || $dados['acao'] == 'excluir') {
    $registros = $dados['dados'];
    foreach ($registros as $value) {
        ?>
        <script>
            $(document).ready(function () {

                $("#Acao").val("<?= $dados['acao']; ?>");
                $("#idRegistro").val("<?= $value['id']; ?>");
                if ("<?= $dados['acao']; ?>" == "editar") {
        <?php
        foreach ($value as $key => $valor) {
            if ($key == "ativo" && $valor == 0) {
                echo "$('#$key').attr('checked', false);";
            } else {
                echo "$('#$key').val('$valor');";
            }
        }
        ?>

                } else {
                    $(".formulario").html("Confirma a exclusão do registro?");
                }
            });
        </script>
        <?php
    }
}
?>



<form id="formulario"  enctype="multipart/form-data">
    <input value="<?= HOST ?>/post/<?= $dados['nm_class_mani']; ?>/gravar.html" id="url" type="hidden">
    <input value="novo" id="Acao" name="Acao" type="hidden">
    <input value="0" id="idRegistro" name="idRegistro" type="hidden">

    <span class="formulario">
        <div class="form-group col-md-12">
            <label for="nome">Razão</label>
            <input type="text" class="form-control text-uppercase" maxlength="45" id="nome" name="nome" placeholder="Nome da Empresa">
        </div>
        <div class="form-group col-md-12">
            <label for="apelido">Fantasia</label>
            <input type="text" class="form-control text-uppercase" maxlength="40" id="apelido" name="apelido" placeholder="Nome da Empresa">
        </div>
        
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="cidade">Cidade</label>
                <input type="text" class="form-control text-uppercase" maxlength="20"  id="cidade" name="cidade" placeholder="Cidade">
            </div>
            <div class="form-group col-md-6">
                <label for="celular">Celular</label>
                <input type="text" class="form-control upper-info" maxlength="16" id="celular" name="celular" placeholder="Celular">
            </div> 
        </div>
        
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="telefone1">Telefone 1</label>
                <input type="text" class="form-control upper-info" maxlength="16"  id="telefone1" name="telefone1" placeholder="Telefone1">
            </div>
            <div class="form-group col-md-6">
                <label for="telefone2">Telefone 2</label>
                <input type="text" class="form-control upper-info" maxlength="16" id="telefone2" name="telefone2" placeholder="Telefone2">
            </div>
        </div>
        <div class="form-group">
            <label for="observacao">Observação</label>
            <textarea class="form-control text-uppercase" id="observacao" name="observacao" rows="3"></textarea>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="ativo" name="ativo" checked="true">
            <label class="form-check-label" for="ativo" value="true">
                Ativo
            </label>
        </div>
    </span>
</form>