<?php
//var_dump($dados);
$img = "assets/img/theme/icon.png";
$cropper = new Cropper("cache");
//$cropper->flush();


if (isset($dados['acao']) && $dados['acao'] == 'editar' || $dados['acao'] == 'excluir') {
    $registros = $dados['dados'];
    foreach ($registros as $value) {
        $img = $value['imagem'] == null ? "assets/img/theme/icon.png" : $value['imagem'];
        ?>
        <script>
            $(document).ready(function () {

                $("#acao").val("<?= $dados['acao']; ?>");
                $("#idRegistro").val("<?= $value['id']; ?>");
                if ("<?= $dados['acao']; ?>" == "editar") {
        <?php
        foreach ($value as $key => $valor) {
            if ($key == "ativo" && $valor == 0) {
                echo "$('#$key').attr('checked', false);";
            } else {
                echo "$('#$key').val('$valor');";
            }
        }
        ?>

                } else {
                    $(".formulario").html("Confirma a exclusão do registro?");
                }
            });
        </script>
        <?php
    }
}
?>




<!-- Page content-->
<div class="container-fluid">
    <div class="row ">
        <div class="card-body col-xl-14">
            <form id="formulario"  enctype="multipart/form-data">
                <input value="<?= HOST ?>/post/<?= $dados['nm_class_mani']; ?>/gravar.html" id="url" type="hidden">
                <input value="novo" id="acao" name="acao" type="hidden">
                <input value="0" id="idRegistro" name="idRegistro" type="hidden">
                <span class="formulario">
                    <h6 class="heading-small text-muted mb-4">Dados do Usuario</h6>
                    <div class="pl-lg-4">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-username">Nome</label>
                                    <input type="text" name='nome' id="nome" class="form-control" placeholder="Username">
                                </div>
                            </div> 
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="senha">senha</label>
                                    <input class="form-control" name='senha' type="password" id="senha" placeholder="••••••">
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-14">
                            <div class="form-group">
                                <label class="form-control-label" for="email">Email</label>
                                <input type="email" name='email'  id="email" class="form-control" placeholder="email">
                            </div>
                        </div>
                        <div class="form-row col-md-12">
                            <div class="form-group col-md-12">
                                <div class="form-group">
                                    <input type="file" class="form-control-file" name="img" id="img">
                                </div>
                                <img src="<?= HOST . "/" . $cropper->Make($img, 150, 150); ?>"  class="rounded" width="150px" height="150px">
                            </div>
                        </div>




                        <div class="form-group ml-0">

                            <div class="form-check">
                                <h3 class="mb-0">Ativo </h3>
                                <label class="custom-toggle">
                                    <input class="form-check-input bg-info" type="checkbox" id="ativo" checked="true" name="ativo">
                                    <span class="custom-toggle-slider rounded-circle" data-label-off="não" data-label-on="sim"></span>

                                </label>
                            </div>
                        </div>
                    </div>
                </span>
            </form>
        </div> 
    </div>                            
</div>                            



