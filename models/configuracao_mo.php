<?php

class configuracao_mo {

    protected $conexao;
    protected $tbl = "configuracao";
    private $mensagens;
    private $select;
   

    public function __construct() {
        $this->mensagens = new Mensagens();
        $this->select = new Select();
    }
    public function Listar($id = null, $ativo = null) {
        $condicao = [];

        if ($id <> null) {
            $condicao = ['id', '=', $id];
        }

        if ($ativo == "S") {
            count($condicao) > 0 ? array_push($condicao, 'AND', 'ativo', '=', '1') : array_push($condicao, 'ativo', '=', '1');
        }
        

        $resultado = $this->select->SelectP(NM_COL[$this->tbl], $this->tbl, $condicao, "order by id asc");
        return $resultado;
    }

    public function Gravar(array $dados) {
        $erros = array();
        //Verifica se é para cadastrar

        if (empty($dados['tempo']) || $dados['tempo'] == "") {
            array_push($erros, $this->mensagens->Msg('nmVazio'));
        }

        ///Imprime os erros para o usuario
        if (count($erros) > 0) {
            echo json_encode($erros);
            exit();
        }

         $condicao = $dados['Acao'] == 'editar' ? ['tempo', '=', $dados['tempo'], 'and', 'id', '<>', $dados['idRegistro']] : ['tempo', '=', $dados['tempo']];
        if ($this->select->Count($this->tbl, $condicao) > 0) {
            array_push($erros, $this->mensagens->Msg('nmClasseExiste'));
            echo json_encode($erros);
            exit();
        }      

        //var_dump($dados);
//        isset($dados['ativo']) ? $dados['ativo'] = 1 : $dados['ativo'] = 0;
        $acao = $dados['Acao'];
        $idRegistro = $dados['idRegistro'];


        unset($dados['Acao']);
        unset($dados['idRegistro']);


        //Remove os campos que não existem no banco de dados
        $nm_colunas = array_keys($dados);
        $nm_valores = array_values($dados);


        if ($acao == 'novo') {
            //Estancia o insert
            $i = new Insert();
            //Cria a conexão com o banco
            $con = new Database();
            //Inicia a transação
            $trans = $con->Begin();
            //Cria a classe de insert       
            if ($i->Insere($trans, $nm_colunas, $nm_valores, $this->tbl)) {
                //faz o commit
                $con->Commit();
                array_push($erros, $this->mensagens->Msg('inSucesso'));
            } else {
                array_push($erros, $this->mensagens->Msg('inErro'));
                $con->Rollback();
            }
        }
        if ($acao == 'editar') {
            //Estancia o insert
            $u = new Update();
            //Cria a conexão com o banco
            $con = new Database();
            //Inicia a transação
            $trans = $con->Begin();
            //echo $idRegistro;
            //Cria a classe de insert       
            if ($u->Atualizar($trans, $nm_colunas, $nm_valores, $this->tbl, ["id", "=", $idRegistro])) {
                array_push($erros, $this->mensagens->Msg('atSucesso'));
                //faz o commit
                $con->Commit();
            } else {
                array_push($erros, $this->mensagens->Msg('inErro'));
                $con->Rollback();
            }
        }

        echo json_encode($erros);
    }

    public function Excluir($id) {       
        $delete = new Delete();
        $conexao = new Database();
        $error = [];
        if ($delete->Deletar($conexao->Begin(), $this->tbl, ['id', '=', $id])) {
            $conexao->Commit();
            array_push($error, $this->mensagens->Msg('exSucesso'));
        } else {
            array_push($error, $this->mensagens->Msg('regNPSE'));
            $conexao->Rollback();
        }
        echo json_encode($error);
    }

}
