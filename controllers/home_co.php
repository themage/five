<?php

class Home_co {

    private $autoLoad;
    private $mensagens;
    private $model;
    private $tabela = "menu";
    private $registros;

    public function __construct() {
        //$this->u = new Usuario_mo();
        $this->autoLoad = new Automacao();
        //$this->model = new Menu_mo();
        $this->registros['nm_class_mani'] = "menu";
    }

    public function Index($p = null) {
        $this->autoLoad->AutoLoad('home');
    }

    public function Erro($p = null) {
        //$this->autoLoad->AutoLoad("tela_erro");
        require_once 'pages/tela_erro.php';
    }

}
