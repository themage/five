<?php

class atendimento_co {

    private $autoLoad;
    private $model;
    private $registros;

    public function __construct() {
        $this->autoLoad = new Automacao();
        $this->model = new atendimento_mo();
        $this->registros['nm_class_mani'] = "atendimento";
    }

    public function Index($p = null) {
        //var_dump($this->model->Listar());
        $this->registros['listagem'] = $this->model->Listar_cliente();
        $this->registros['titulo'] = 'Atendimento';
        $this->autoLoad->AutoLoad($this->registros['nm_class_mani'] . '_listar', $this->registros);
    }

    public function Sair() {
        // $dados = filter_input_array(INPUT_POST, FILTER_DEFAULT);
        $this->model->Sair();
    }

    public function Acao($p = null) {
        $empresa = new cliente_mo();
        $this->registros['cliente'] = $empresa->Listar(null, 'S');

        $this->registros['titulo'] = 'Atendimento';
        $this->registros['acao'] = $p[0];
        if ($this->registros['acao'] <> 'novo') {
            //Atribui o id do registro
            isset($p[1]) ? $this->registros['idRegistro'] = $p[1] : $this->registros['idRegistro'] = null;
            //Faz o select do item
            $this->registros['idRegistro'] <> null ? $this->registros['dados'] = $this->model->Listar($this->registros['idRegistro']) : 0;
        }

        $this->autoLoad->AutoLoad($this->registros['nm_class_mani'] . '_acao', $this->registros);
    }

    public function Gravar($p = null) {
        $dados = filter_input_array(INPUT_POST, FILTER_DEFAULT);
        $link = $_FILES;

        //var_dump($dados);
        if ($dados['Acao'] == "novo" || $dados['Acao'] == "editar") {
            $this->model->Gravar($dados, $link);
        }
        if ($dados['Acao'] == 'excluir') {
            $this->model->Excluir($dados['idRegistro']);
        }
    }

}

?>
