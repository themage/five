<?php

class Usuario_co {

    private $autoLoad;
    private $mensagens;
    private $model;
    private $tabela = "usuario";
    private $registros;

    public function __construct() {
        //$this->u = new Usuario_mo();
        $this->autoLoad = new Automacao();
        $this->model = new usuario_mo();
        $this->registros['nm_class_mani'] = "usuario";
    }

    public function Index($p = null) {
        // var_dump($this->model->Listar());
        $this->registros['usuario'] = $this->model->Listar();
        $this->registros['titulo'] = 'usuario';
        $this->autoLoad->AutoLoad($this->registros['nm_class_mani'] . '_listar', $this->registros);
    }

    public function Logar() {
        $dados = filter_input_array(INPUT_POST, FILTER_DEFAULT);
        //var_dump($dados);
        //echo 'Ola';
        $this->model->Logar($dados);
    }

    public function Sair() {
        // $dados = filter_input_array(INPUT_POST, FILTER_DEFAULT);
        $this->model->Sair();
    }

    public function Acao($p = null) {
        $this->registros['titulo'] = 'Usuario';
        $this->registros['acao'] = $p[0];
        if ($this->registros['acao'] <> 'novo') {
            //Atribui o id do registro
            //var_dump($this->registros);
            
            isset($p[1]) ? $this->registros['idRegistro'] = $p[1] : $this->registros['idRegistro'] = null;
            //Faz o select do item
            $this->registros['idRegistro'] <> null ? $this->registros['dados'] = $this->model->Listar($this->registros['idRegistro']) : 0;
        }
       
        //Seleciona as classes de acessos
       //$ca = new Classe_acesso_mo();
        //$this->registros['classeacessoid'] = $ca->Listar(null, 'S');
        $this->autoLoad->AutoLoad($this->registros['nm_class_mani'] . '_acao', $this->registros);
    }

    public function Perfil($p = null) {
        $this->registros['titulo'] = 'Meu perfil';
        $this->registros['acao'] = 'editar';

        //Atribui o id do registro
        isset($p[1]) ? $this->registros['idRegistro'] = $p[1] : $this->registros['idRegistro'] = null;
        //Faz o select do item
        $this->registros['dados'] = $this->model->Listar($_SESSION['frlIdUser']);
       // var_dump($this->registros['dados']);
        //Seleciona as classes de acessos
        $ca = new Classe_acesso_mo();
         $this->registros['alterarClasseAcesso'] = true;
        $this->registros['classeacessoid'] = $ca->Listar(null, 'S');
        $this->autoLoad->AutoLoad($this->registros['nm_class_mani'] . '_acao', $this->registros);
    }

    public function Gravar($p = null) {        
       $dados = filter_input_array(INPUT_POST, FILTER_DEFAULT);
       $img = $_FILES;
       //var_dump($dados);
        if ($dados['acao'] == 'novo' || $dados['acao'] == 'editar') {
            $this->model->Gravar($dados, $img);
            
        }
        if ($dados['acao'] == 'excluir') {
            $this->model->Gravar($dados);
            
        }
    }

}

?>